#include <stream9/xdg/environment.hpp>

#include "namespace.hpp"

#include <stream9/filesystem/load_string.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(environment_)

    BOOST_AUTO_TEST_CASE(save_file_)
    {
        using stream9::path::operator/;

        fs::temporary_directory dir;

        auto fname = dir.path() / "foo";

        stream9::xdg::env().save_file(fname, "hello world");

        auto s = fs::load_string(fname);

        BOOST_TEST(s == "hello world");
    }

BOOST_AUTO_TEST_SUITE_END() // environment_

} // namespace testing
