#ifndef STREAM9_XDG_APPLICATION_TEST_SRC_APPLICATION_MAKE_PATH_HPP
#define STREAM9_XDG_APPLICATION_TEST_SRC_APPLICATION_MAKE_PATH_HPP

#include "../namespace.hpp"

#include <stream9/path/concat.hpp> // operator/
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <type_traits>

namespace testing {

using path::operator/;
using st9::string;
using st9::string_view;

template<typename S1, typename S2>
    requires std::is_convertible_v<S1, string_view>
          && std::is_convertible_v<S2, string_view>
inline string
make_path(S1&& parent, S2&& p)
{
    return string_view(parent) / string_view(p);
}

template<typename S1, typename S2, typename... Rest>
    requires std::is_convertible_v<S1, string_view>
          && std::is_convertible_v<S2, string_view>
inline string
make_path(S1&& parent, S2&& p, Rest&&... rest)
{
    return make_path(parent, p) + ":" + make_path(parent, std::forward<Rest>(rest)...);
}

} // namespace testing

#endif // STREAM9_XDG_APPLICATION_TEST_SRC_APPLICATION_MAKE_PATH_HPP
