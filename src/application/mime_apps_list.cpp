#include "src/application/mime_apps_list.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/tmpfstream.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/string.hpp>
#include <stream9/strings/stream.hpp>

namespace testing {

using stream9::strings::operator<<;
using stream9::string;

BOOST_AUTO_TEST_SUITE(mime_apps_list_)

    BOOST_AUTO_TEST_SUITE(added_associations_)

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            fs::tmpfstream os;

            os << "[Added Associations]\n"
               << "foo/bar=foo.desktop;bar.desktop\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.added_associations("xxx/yyy");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_empty_)
        {
            fs::tmpfstream os;

            os << "[Added Associations]\n"
               << "foo/bar=\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.added_associations("foo/bar");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_one_id_)
        {
            fs::tmpfstream os;

            os << "[Added Associations]\n"
               << "foo/bar=foo.desktop;\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.added_associations("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(found_two_ids_)
        {
            fs::tmpfstream os;

            os << "[Added Associations]\n"
               << "foo/bar=foo.desktop;bar.desktop;\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.added_associations("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop", "bar.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(real_data_)
        {
            using stream9::path::operator/;

            auto const dir = data_dir() / "real_data" / "config_home";
            apps::mime_apps_list list { dir / "mimeapps.list" };

            auto ids = list.added_associations("video/mp4");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = {
                "mpv.desktop",
                "mplayer.desktop",
            };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // added_associations_

    BOOST_AUTO_TEST_SUITE(removed_associations_)

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            fs::tmpfstream os;

            os << "[Removed Associations]\n"
               << "foo/bar=foo.desktop;bar.desktop\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.removed_associations("xxx/yyy");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_empty_)
        {
            fs::tmpfstream os;

            os << "[Removed Associations]\n"
               << "foo/bar=\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.removed_associations("foo/bar");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_one_id_)
        {
            fs::tmpfstream os;

            os << "[Removed Associations]\n"
               << "foo/bar=foo.desktop;\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.removed_associations("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(found_two_ids_)
        {
            fs::tmpfstream os;

            os << "[Removed Associations]\n"
               << "foo/bar=foo.desktop;bar.desktop;\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.removed_associations("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop", "bar.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(real_data_)
        {
            using stream9::path::operator/;

            auto const dir = data_dir() / "real_data" / "config_home";
            apps::mime_apps_list list { dir / "mimeapps.list" };

            auto ids = list.removed_associations("video/mpeg");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = {
                "smplayer_enqueue.desktop",
            };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // removed_associations_

    BOOST_AUTO_TEST_SUITE(default_applications_)

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            fs::tmpfstream os;

            os << "[Default Applications]\n"
               << "foo/bar=foo.desktop;bar.desktop\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.default_applications("xxx/yyy");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_empty_)
        {
            fs::tmpfstream os;

            os << "[Default Applications]\n"
               << "foo/bar=\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.default_applications("foo/bar");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_one_id_)
        {
            fs::tmpfstream os;

            os << "[Default Applications]\n"
               << "foo/bar=foo.desktop;\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.default_applications("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(found_two_ids_)
        {
            fs::tmpfstream os;

            os << "[Default Applications]\n"
               << "foo/bar=foo.desktop;bar.desktop;\n"
               << std::flush;

            apps::mime_apps_list list { os.path().c_str() };

            auto ids = list.default_applications("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop", "bar.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(real_data_)
        {
            using stream9::path::operator/;

            auto const dir = data_dir() / "real_data" / "config_home";
            apps::mime_apps_list list { dir / "mimeapps.list" };

            auto ids = list.default_applications("video/mp4");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = {
                "mpv.desktop",
            };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // default_applications_

    BOOST_AUTO_TEST_SUITE(add_association_)

        BOOST_AUTO_TEST_CASE(add_one_)
        {
            fs::tmpfstream os;
            apps::mime_apps_list list { os.path() };

            BOOST_TEST(list.empty());

            list.add_association("xxx/yyy", "foo.desktop");

            auto expected = "[Added Associations]\n"
                            "xxx/yyy=foo.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_two_apps_1_)
        {
            fs::tmpfstream os;
            apps::mime_apps_list list { os.path() };

            list.add_association("xxx/yyy", "foo.desktop");
            list.add_association("xxx/yyy", "bar.desktop");

            auto expected = "[Added Associations]\n"
                            "xxx/yyy=foo.desktop;bar.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_two_apps_2_)
        {
            fs::tmpfstream os;
            apps::mime_apps_list list { os.path() };

            list.add_association("xxx/yyy", "foo.desktop");
            list.add_association("yyy/zzz", "xyzzy.desktop");

            auto expected = "[Added Associations]\n"
                            "xxx/yyy=foo.desktop\n"
                            "yyy/zzz=xyzzy.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(swap_order_by_adding_app_)
        {
            fs::tmpfstream os;
            os << "[Added Associations]\n"
                  "xxx/yyy=foo.desktop;bar.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.add_association("xxx/yyy", "bar.desktop");

            auto expected = "[Added Associations]\n"
                            "xxx/yyy=bar.desktop;foo.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_removed_app_)
        {
            fs::tmpfstream os;
            os << "[Removed Associations]\n"
                  "xxx/yyy=foo.desktop;bar.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.add_association("xxx/yyy", "foo.desktop");

            auto expected = "[Added Associations]\n"
                            "xxx/yyy=foo.desktop\n"
                            "\n"
                            "[Removed Associations]\n"
                            "xxx/yyy=bar.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // add_association_

    BOOST_AUTO_TEST_SUITE(add_default_application_)

        BOOST_AUTO_TEST_CASE(add_one_1_)
        {
            fs::tmpfstream os;

            apps::mime_apps_list list { os.path() };

            list.add_default_application("xxx/yyy", "foo.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=foo.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_one_2_)
        {
            fs::tmpfstream os;
            os << "[Default Applications]\n"
                  "xxx/yyy=foo.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.add_default_application("xxx/yyy", "foo.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=foo.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_one_3_)
        {
            fs::tmpfstream os;
            os << "[Default Applications]\n"
                  "xxx/yyy=foo.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.add_default_application("xxx/yyy", "bar.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=bar.desktop;foo.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_one_4_)
        {
            fs::tmpfstream os;
            os << "[Default Applications]\n"
                  "xxx/yyy=bar.desktop;foo.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.add_default_application("xxx/yyy", "foo.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=foo.desktop;bar.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_two_1_)
        {
            fs::tmpfstream os;

            apps::mime_apps_list list { os.path() };

            list.add_default_application("xxx/yyy", "foo.desktop");
            list.add_default_application("xxx/yyy", "bar.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=bar.desktop;foo.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(add_two_2_)
        {
            fs::tmpfstream os;

            apps::mime_apps_list list { os.path() };

            list.add_default_application("xxx/yyy", "foo.desktop");
            list.add_default_application("yyy/zzz", "bar.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=foo.desktop\n"
                            "yyy/zzz=bar.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // add_default_application_

    BOOST_AUTO_TEST_SUITE(remove_default_application_)

        BOOST_AUTO_TEST_CASE(empty_)
        {
            fs::tmpfstream os;

            apps::mime_apps_list list { os.path() };

            list.remove_default_application("xxx/yyy", "foo.desktop");

            BOOST_TEST(list.empty());
        }

        BOOST_AUTO_TEST_CASE(remove_1_)
        {
            fs::tmpfstream os;
            os << "[Default Applications]\n"
                  "xxx/yyy=bar.desktop;foo.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.remove_default_application("xxx/yyy", "foo.desktop");

            auto expected = "[Default Applications]\n"
                            "xxx/yyy=bar.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(remove_2_)
        {
            fs::tmpfstream os;
            os << "[Default Applications]\n"
                  "xxx/yyy=foo.desktop\n"
                  "yyy/zzz=bar.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.remove_default_application("xxx/yyy", "foo.desktop");

            auto expected = "[Default Applications]\n"
                            "yyy/zzz=bar.desktop\n";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

        BOOST_AUTO_TEST_CASE(remove_3_)
        {
            fs::tmpfstream os;
            os << "[Default Applications]\n"
                  "xxx/yyy=foo.desktop\n" << std::flush;

            apps::mime_apps_list list { os.path() };

            list.remove_default_application("xxx/yyy", "foo.desktop");

            auto expected = "";

            string s; s << list;
            BOOST_TEST(s == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // remove_default_application_

BOOST_AUTO_TEST_SUITE_END() // mime_apps_list_

} // namespace testing
