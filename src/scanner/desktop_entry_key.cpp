#include "../../../src/scanner/desktop_entry_key.hpp"

#include "../namespace.hpp"

#include <string>
#include <string_view>
#include <system_error>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(scanner_)
BOOST_AUTO_TEST_SUITE(desktop_entry_key_)

    namespace scanner = stream9::xdg::scanner;

    class event_recorder
    {
    public:
        using next_action = scanner::next_action;

    public:
        event_recorder(std::string_view text)
            : m_text { text }
        {}

        void on_name(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "name: " << s;

            m_events.emplace_back(message);
        }

        void on_locale(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "locale: " << s;

            m_events.emplace_back(message);
        }

        void on_lang(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "lang: " << s;

            m_events.emplace_back(message);
        }

        void on_country(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "country: " << s;

            m_events.emplace_back(message);
        }

        void on_encoding(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "encoding: " << s;

            m_events.emplace_back(message);
        }

        void on_modifier(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "modifier: " << s;

            m_events.emplace_back(message);
        }

        void on_error(std::error_code const& e,
                      std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "error: " << e.message() << " at [" << s.begin() - m_text.begin()
               << ", " << s.end() - m_text.begin() << "]";

            m_events.emplace_back(message);
        }

        auto const& events() const { return m_events; }

    private:
        std::string_view m_text;
        std::vector<std::string> m_events;
    };

    BOOST_AUTO_TEST_CASE(basic_usage_)
    {
        auto const text = "foo[en_US.UTF-8@mod]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: foo",
            "lang: en",
            "country: US",
            "encoding: UTF-8",
            "modifier: mod",
            "locale: en_US.UTF-8@mod",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = "";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "error: empty name at [0, 0]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(no_locale_)
    {
        auto const text = "foo";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: foo",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(no_close_branket_)
    {
        auto const text = "foo[en";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: foo",
            "lang: en",
            "locale: en",
            "error: terminator is expected at [6, 6]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(excess_char_on_tail_)
    {
        auto const text = "foo[en]  ";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: foo",
            "lang: en",
            "locale: en",
            "error: end of string is expected at [7, 9]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_name_)
    {
        auto const text = "[en]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "error: empty name at [0, 0]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(illegal_name_)
    {
        auto const text = "name+1";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "error: invalid character at [4, 5]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_locale_)
    {
        auto const text = "name[]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "error: empty lang at [5, 5]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_lang_1_)
    {
        auto const text = "name[_US]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "error: empty lang at [5, 5]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_lang_2_)
    {
        auto const text = "name[.UTF-8]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "error: empty lang at [5, 5]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_lang_3_)
    {
        auto const text = "name[@mod]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "error: empty lang at [5, 5]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_lang_4_)
    {
        auto const text = "name[";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "error: empty lang at [5, 5]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(invalid_lang_)
    {
        auto const text = "name[en!]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "error: invalid character at [7, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_country_1_)
    {
        auto const text = "name[en_]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: empty country at [8, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_country_2_)
    {
        auto const text = "name[en_.]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: empty country at [8, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_country_3_)
    {
        auto const text = "name[en_@]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: empty country at [8, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_country_4_)
    {
        auto const text = "name[en_";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: empty country at [8, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(invalid_country_)
    {
        auto const text = "name[en_US;]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: invalid character at [10, 11]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_encoding_1_)
    {
        auto const text = "name[en_US.]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "country: US",
            "error: empty encoding at [11, 11]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_encoding_2_)
    {
        auto const text = "name[en_US.@]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "country: US",
            "error: empty encoding at [11, 11]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_encoding_3_)
    {
        auto const text = "name[en_US.";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "country: US",
            "error: empty encoding at [11, 11]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(invalid_encoding_)
    {
        auto const text = "name[en_US.UTF=8]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "country: US",
            "error: invalid character at [14, 15]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_modifier_1_)
    {
        auto const text = "name[en@]";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: empty modifier at [8, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_modifier_2_)
    {
        auto const text = "name[en@";

        event_recorder h { text };
        scanner::desktop_entry_key::scan(text, h);

        auto const expected = {
            "name: name",
            "lang: en",
            "error: empty modifier at [8, 8]",
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // desktop_entry_key_
BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace testing
