#ifndef STREAM9_DESKTOP_SRC_SCANNER_DESKTOP_ENTRY_HADLER_HPP
#define STREAM9_DESKTOP_SRC_SCANNER_DESKTOP_ENTRY_HADLER_HPP

#include "../namespace.hpp"

#include <stream9/desktop/scanner.hpp>
#include <stream9/strings/stream.hpp>

namespace testing {

using desktop::parser::command;
using desktop::parser::errc;

class desktop_entry_handler : public desktop::parser::desktop_entry_handler
{
public:
    struct event {
        int line_no;
        std::string message;

        bool operator==(event const&) const = default;
    };

public:
    desktop_entry_handler(std::string_view const text)
        : m_text { text }
    {}

    command on_group_header(std::string_view const name) override
    {
        desktop::parser::desktop_entry_handler::on_group_header(name);

        std::string message;
        str::ostream os { message };
        os << "group header: " << name;

        m_events.emplace_back(line_no(), message);

        return {};
    }

    command on_entry(std::string_view const key,
                     std::string_view const locale,
                     std::string_view const value) override
    {
        std::string message;
        str::ostream os { message };
        os << "entry: " << key << ", " << locale << ", " << value;

        m_events.emplace_back(line_no(), message);

        return {};
    }

    command on_comment(std::string_view const text) override
    {
        std::string message;
        str::ostream os { message };
        os << "comment: " << text;

        m_events.emplace_back(line_no(), message);

        return {};
    }

    command on_error(errc const e, std::string_view const range) override
    {
        std::string message;
        str::ostream os { message };
        os << "error: " << e << ", [" << range.begin() - m_bol
           << ", " << range.end() - m_bol << "]";

        m_events.emplace_back(line_no(), message);

        return {};
    }

    void on_new_line(std::string_view::iterator const it) override
    {
        desktop::parser::desktop_entry_handler::on_new_line(it);
        m_bol = it + 1;
    }

    auto const& events() const { return m_events; }

    friend std::ostream& operator<<(std::ostream& os, event const& e)
    {
        return os << e.line_no << ": " << e.message;
    }

private:
    std::string_view m_text;
    std::string_view::iterator m_bol;
    std::vector<event> m_events;
};

} // namespace testing

#endif // STREAM9_DESKTOP_SRC_DESKTOP_SCANNER_ENTRY_HADLER_HPP
