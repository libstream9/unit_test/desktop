#include "../../../src/scanner/exec_string.hpp"

#include "../namespace.hpp"

#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

constexpr boost::test_tools::per_element per_element;

namespace desktop = stream9::xdg;

class event_recorder
{
public:
    using next_action = desktop::scanner::next_action;
    using iterator = desktop::scanner::exec_string::iterator;

public:
    event_recorder(std::string_view const s)
        : m_text { s }
    {}

    void begin_argument(next_action&)
    {
        m_events.push_back("begin argument");
    }

    void end_argument(next_action&)
    {
        m_events.push_back("end argument");
    }

    void on_character(char const c, next_action&)
    {
        std::string ev = "character: [";
        ev.push_back(c);
        ev.append("]");
        m_events.push_back(std::move(ev));
    }

    void on_error(std::error_code const& ec, iterator const it, next_action&)
    {
        std::string ev = "error: ";
        str::ostream ss { ev };
        auto const loc = it - m_text.begin();

        ss << ec.message() << " at " << loc;

        m_events.push_back(std::move(ev));
    }

    auto const& events() const { return m_events; }

private:
    std::string_view m_text;
    std::vector<std::string> m_events;
};

BOOST_AUTO_TEST_SUITE(scanner_)
BOOST_AUTO_TEST_SUITE(exec_string_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = "";

        event_recorder rec { text };

        desktop::scanner::exec_string::scan(text, rec);

        BOOST_TEST(rec.events().empty());
    }

    BOOST_AUTO_TEST_CASE(a_unquoted_argument_)
    {
        auto const text = "  abc";

        event_recorder rec { text };

        desktop::scanner::exec_string::scan(text, rec);

        auto const expected = {
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "end argument",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(two_unquoted_arguments)
    {
        auto const text = "abc\t xyz";

        event_recorder rec { text };

        desktop::scanner::exec_string::scan(text, rec);

        auto const expected = {
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "end argument",
            "begin argument",
            "character: [x]",
            "character: [y]",
            "character: [z]",
            "end argument",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(a_quoted_argument_)
    {
        auto const text = "\" abc\\\"\"";

        event_recorder rec { text };

        desktop::scanner::exec_string::scan(text, rec);

        auto const expected = {
            "begin argument",
            "character: [ ]",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "character: [\"]",
            "end argument",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(unquoted_and_quoted_argument_)
    {
        auto const text = "xyz \"abc\"";

        event_recorder rec { text };

        desktop::scanner::exec_string::scan(text, rec);

        auto const expected = {
            "begin argument",
            "character: [x]",
            "character: [y]",
            "character: [z]",
            "end argument",
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "end argument",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(two_unquoted_arguments_)
    {
        auto const text = "\"xyz\" \"abc\"";

        event_recorder rec { text };

        desktop::scanner::exec_string::scan(text, rec);

        auto const expected = {
            "begin argument",
            "character: [x]",
            "character: [y]",
            "character: [z]",
            "end argument",
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "end argument",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_)
    {
        auto const text = "\"abc";

        event_recorder rec { text };

        auto const ok = desktop::scanner::exec_string::scan(text, rec);
        BOOST_TEST(!ok);

        auto const expected = {
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "error: unclosed quote at 4",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(unescaped_char_)
    {
        auto const text = "\"abc$\"";

        event_recorder rec { text };

        auto const ok = desktop::scanner::exec_string::scan(text, rec);
        BOOST_TEST(!ok);

        auto const expected = {
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "error: unescaped char at 4",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(unknown_escape_sequence_)
    {
        auto const text = "\"abc\\x0d\"";

        event_recorder rec { text };

        auto const ok = desktop::scanner::exec_string::scan(text, rec);
        BOOST_TEST(!ok);

        auto const expected = {
            "begin argument",
            "character: [a]",
            "character: [b]",
            "character: [c]",
            "error: unknown escape sequence at 4",
        };

        BOOST_TEST(rec.events() == expected, per_element);
    }

BOOST_AUTO_TEST_SUITE_END() // exec_string_
BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace testing
