#include "../../../src/scanner/locale.hpp"

#include "../namespace.hpp"

#include <string>
#include <string_view>
#include <system_error>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

constexpr boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(scanner_)
BOOST_AUTO_TEST_SUITE(locale_)

    namespace scanner = stream9::xdg::scanner;

    class event_recorder
    {
    public:
        using next_action = scanner::next_action;

    public:
        event_recorder(std::string_view text)
            : m_text { text }
        {}

        void on_locale(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "locale: " << s;

            m_events.emplace_back(message);
        }

        void on_lang(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "lang: " << s;

            m_events.emplace_back(message);
        }

        void on_country(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "country: " << s;

            m_events.emplace_back(message);
        }

        void on_encoding(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "encoding: " << s;

            m_events.emplace_back(message);
        }

        void on_modifier(std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "modifier: " << s;

            m_events.emplace_back(message);
        }

        void on_error(std::error_code const& e,
                      std::string_view const s, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "error: " << e.message() << " at [" << s.begin() - m_text.begin()
               << ", " << s.end() - m_text.begin() << "]";

            m_events.emplace_back(message);
        }

        auto const& events() const { return m_events; }

    private:
        std::string_view m_text;
        std::vector<std::string> m_events;
    };

    BOOST_AUTO_TEST_CASE(basic_usage_)
    {
        auto const text = "en_US.UTF-8@mod";

        event_recorder h { text };
        scanner::locale::scan(text, h);

        auto const expected = {
            "lang: en",
            "country: US",
            "encoding: UTF-8",
            "modifier: mod",
            "locale: en_US.UTF-8@mod",
        };

        BOOST_TEST(h.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = "";

        event_recorder h { text };
        scanner::locale::scan(text, h);

        auto const expected = {
            "error: empty lang at [0, 0]",
        };

        BOOST_TEST(h.events() == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(with_custom_end_tester_)
    {
        auto const text = "en_US.UTF-8@mod]";

        event_recorder h { text };
        auto it = str::begin(text);
        scanner::locale::scan(it, str::end(text), h,
            [](auto it, auto end) {
                return it == end || *it == ']';
            } );

        auto const expected = {
            "lang: en",
            "country: US",
            "encoding: UTF-8",
            "modifier: mod",
            "locale: en_US.UTF-8@mod",
        };

        BOOST_TEST(h.events() == expected, per_element);
    }

    // more tests in scanner::desktop_entry_key

BOOST_AUTO_TEST_SUITE_END() // locale_
BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace testing
