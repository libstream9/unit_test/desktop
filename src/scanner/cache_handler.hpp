#include "../namespace.hpp"

#include <stream9/desktop/scanner.hpp>

#include <ostream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

namespace testing {

using desktop::parser::mime_cache_handler;
using desktop::parser::command;
using desktop::parser::errc;

class cache_handler : public mime_cache_handler
{
public:
    struct entry {
        std::string_view mime_type;
        std::string_view ids;
        int line_no;

        bool operator==(entry const&) const = default;
    };

    friend std::ostream& operator<<(std::ostream& os, entry const& e)
    {
        return os << e.line_no << ": " << e.mime_type << ", " << e.ids;
    }

    struct comment {
        std::string_view text;
        int line_no;

        bool operator==(comment const&) const = default;
    };

    struct error {
        errc e;
        std::string_view text;
        int line_no;

        bool operator==(error const&) const = default;
    };

public:
    command on_entry(std::string_view const mime_type,
                  std::string_view const desktop_file_ids)
    {
        m_entries.push_back({
            mime_type, desktop_file_ids, line_no()
        });

        return {};
    }

    command on_comment(std::string_view const text) override
    {
        m_comments.push_back({
            text,
            line_no(),
        });

        return {};
    }

    command on_error(errc e, std::string_view const text) override
    {
        m_errors.push_back({
            e,
            text,
            line_no(),
        });

        return {};
    }

    auto const& entries() const { return m_entries; }
    auto const& comments() const { return m_comments; }
    auto const& errors() const { return m_errors; }

private:
    std::vector<entry> m_entries;
    std::vector<comment> m_comments;
    std::vector<error> m_errors;
};

} // namespace testing
