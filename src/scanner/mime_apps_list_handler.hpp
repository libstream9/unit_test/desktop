#include "../namespace.hpp"

#include <stream9/desktop/scanner.hpp>

#include <ostream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

namespace testing {

namespace parser = desktop::parser;

using parser::command;
using parser::errc;

class mime_apps_list_handler : public parser::mime_apps_list_handler
{
public:
    struct event {
        int line_no;
        std::string message;

        bool operator==(event const&) const = default;
    };

public:
    mime_apps_list_handler(std::string_view const text)
        : m_bol { text.begin() }
    {}

    command on_added_associations(std::string_view)
    {
        std::string message;
        str::ostream os { message };
        os << "added associations group";

        m_events.emplace_back(m_line_no, message);

        return {};
    }

    command on_removed_associations(std::string_view)
    {
        std::string message;
        str::ostream os { message };
        os << "removed associations group";

        m_events.emplace_back(m_line_no, message);

        return {};
    }

    command on_default_applications(std::string_view)
    {
        std::string message;
        str::ostream os { message };
        os << "default applications group";

        m_events.emplace_back(m_line_no, message);

        return {};
    }

    command on_entry(std::string_view const mime_type,
                  std::string_view const desktop_file_ids)
    {
        std::string message;
        str::ostream os { message };
        os << "entry: " << mime_type << ", " << desktop_file_ids;

        m_events.emplace_back(m_line_no, message);

        return {};
    }

    command on_comment(std::string_view const text) override
    {
        std::string message;
        str::ostream os { message };
        os << "comment: " << text;

        m_events.emplace_back(m_line_no, message);

        return {};
    }

    command on_error(errc e, std::string_view const range) override
    {
        std::string message;
        str::ostream os { message };
        os << "error: " << e << ", [" << range.begin() - m_bol
           << ", " << range.end() - m_bol << "]";

        m_events.emplace_back(m_line_no, message);

        return {};
    }

    void on_new_line(std::string_view::iterator const it) override
    {
        ++m_line_no;
        m_bol = it + 1;
    }

    auto const& events() const { return m_events; }

    friend std::ostream& operator<<(std::ostream& os, event const& e)
    {
        return os << e.line_no << ": " << e.message;
    }

private:
    std::vector<event> m_events;
    std::string_view::iterator m_bol;
    int m_line_no = 1;
};

} // namespace testing
