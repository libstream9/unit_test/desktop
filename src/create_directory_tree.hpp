#include "namespace.hpp"

#include <stream9/string_view.hpp>
#include <stream9/json.hpp>

namespace testing {

void create_directory_tree(st9::string_view parent, json::object const& tree);

} // namespace testing
