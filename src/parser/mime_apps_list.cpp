#include "src/parser/mime_apps_list.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/path.hpp>

#include <ostream>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace parser = xdg::parser;

using parser::mime_apps_list::entry_map;
using stream9::error;

BOOST_AUTO_TEST_SUITE(parser_)
BOOST_AUTO_TEST_SUITE(mime_apps_list_)

    struct result {
        entry_map added_assocs;
        entry_map removed_assocs;
        entry_map default_apps;
        st9::array<error> errors;
    };

    auto parse(std::string_view const text)
    {
        result r;

        parser::mime_apps_list::parse(text,
            r.added_assocs,
            r.removed_assocs,
            r.default_apps,
            &r.errors);

        return r;
    }

    BOOST_AUTO_TEST_CASE(real_data_)
    {
        using stream9::path::operator/;

        auto const dir = data_dir() / "real_data" / "config_home";
        auto text = fs::load_string(dir / "mimeapps.list");
        BOOST_TEST(!text.empty());

        auto r = parse(text);

        BOOST_TEST(r.added_assocs.size() == 6);
        BOOST_TEST(r.removed_assocs.size() == 3);
        BOOST_TEST(r.default_apps.size() == 7);
        BOOST_TEST(r.errors.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(empty_data_)
    {
        auto r = parse("");

        BOOST_TEST(r.added_assocs.size() == 0);
        BOOST_TEST(r.removed_assocs.size() == 0);
        BOOST_TEST(r.default_apps.size() == 0);
        BOOST_TEST(r.errors.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(one_entry_)
    {
        auto const text = "[Added Associations]\n"
            "foo/bar=foo.desktop;\n"
            "\n"
            "[Removed Associations]\n"
            "xxx/yyy=xxx.desktop;yyy.desktop\n"
            "\n"
            "[Default Applications]\n"
            "aaa/bbb=bbb.desktop\n"
            ;

        auto r = parse(text);

        entry_map expected_added_assocs = {
            { "foo/bar", { "foo.desktop" } }
        };
        BOOST_TEST(r.added_assocs == expected_added_assocs, boost::test_tools::per_element());

        entry_map expected_removed_assocs = {
            { "xxx/yyy", { "xxx.desktop", "yyy.desktop" } }
        };
        BOOST_TEST(r.removed_assocs == expected_removed_assocs, boost::test_tools::per_element());

        entry_map expected_default_apps = {
            { "aaa/bbb", "bbb.desktop" }
        };
        BOOST_TEST(r.default_apps == expected_default_apps, boost::test_tools::per_element());

        BOOST_TEST(r.errors.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(two_entry_)
    {
        auto const text = "[Added Associations]\n"
            "foo/bar=foo.desktop;\n"
            "xxx/yyy=yyy.desktop;\n"
            "\n"
            "[Removed Associations]\n"
            "xxx/yyy=xxx.desktop;yyy.desktop\n"
            "foo/bar=foo.desktop\n"
            "\n"
            "[Default Applications]\n"
            "aaa/bbb=bbb.desktop\n"
            "xxx/yyy=xxx.desktop;\n"
            ;

        auto r = parse(text);

        entry_map expected_added_assocs = {
            { "foo/bar", { "foo.desktop" } },
            { "xxx/yyy", { "yyy.desktop" } },
        };
        BOOST_TEST(r.added_assocs == expected_added_assocs, boost::test_tools::per_element());

        entry_map expected_removed_assocs = {
            { "xxx/yyy", { "xxx.desktop", "yyy.desktop" } },
            { "foo/bar", { "foo.desktop" } },
        };
        BOOST_TEST(r.removed_assocs == expected_removed_assocs, boost::test_tools::per_element());

        entry_map expected_default_apps = {
            { "aaa/bbb", { "bbb.desktop" } },
            { "xxx/yyy", { "xxx.desktop" } },
        };
        BOOST_TEST(r.default_apps == expected_default_apps, boost::test_tools::per_element());

        BOOST_TEST(r.errors.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(duplicated_mime_type_)
    {
        auto const text = "[Added Associations]\n"
            "foo/bar=foo.desktop;\n"
            "foo/bar=yyy.desktop;\n"
            "\n"
            "[Removed Associations]\n"
            "xxx/yyy=xxx.desktop;yyy.desktop\n"
            "xxx/yyy=foo.desktop\n"
            "\n"
            "[Default Applications]\n"
            "aaa/bbb=bbb.desktop\n"
            "aaa/bbb=xxx.desktop;\n"
            ;

        auto r = parse(text);

        entry_map expected_added_assocs = {
            { "foo/bar", { "foo.desktop" } },
        };
        BOOST_TEST(r.added_assocs == expected_added_assocs, boost::test_tools::per_element());

        entry_map expected_removed_assocs = {
            { "xxx/yyy", { "xxx.desktop", "yyy.desktop" } },
        };
        BOOST_TEST(r.removed_assocs == expected_removed_assocs, boost::test_tools::per_element());

        entry_map expected_default_apps = {
            { "aaa/bbb", { "bbb.desktop" } },
        };
        BOOST_TEST(r.default_apps == expected_default_apps, boost::test_tools::per_element());

        using parser::mime_apps_list::errc;

        BOOST_REQUIRE(r.errors.size() == 3);
        BOOST_TEST(r.errors[0].why() == errc::duplicated_key);
        BOOST_TEST(r.errors[1].why() == errc::duplicated_key);
        BOOST_TEST(r.errors[2].why() == errc::duplicated_key);
    }

    BOOST_AUTO_TEST_CASE(unknown_group_)
    {
        auto const text = "[Added Associations X]\n"
            "foo/bar=foo.desktop;\n"
            "\n"
            "[Removed Associations]\n"
            "xxx/yyy=xxx.desktop;yyy.desktop\n"
            "\n"
            "[Default Applications Y]\n"
            "aaa/bbb=bbb.desktop\n"
            ;

        auto r = parse(text);

        entry_map expected_added_assocs;
        BOOST_TEST(r.added_assocs == expected_added_assocs, boost::test_tools::per_element());

        entry_map expected_removed_assocs = {
            { "xxx/yyy", { "xxx.desktop", "yyy.desktop" } }
        };
        BOOST_TEST(r.removed_assocs == expected_removed_assocs, boost::test_tools::per_element());

        entry_map expected_default_apps;
        BOOST_TEST(r.default_apps == expected_default_apps, boost::test_tools::per_element());

        using parser::mime_apps_list::errc;

        BOOST_REQUIRE(r.errors.size() == 2);
        BOOST_TEST(r.errors[0].why() == errc::invalid_group_name);
        BOOST_TEST(r.errors[1].why() == errc::invalid_group_name);
    }

BOOST_AUTO_TEST_SUITE_END() // mime_apps_list_
BOOST_AUTO_TEST_SUITE_END() // parser_

} // namespace testing
