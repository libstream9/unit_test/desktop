#include "../../src/parser/desktop_entry.hpp"

#include "../namespace.hpp"

#include <stream9/array.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/container/small_vector.hpp>

namespace testing {

namespace parser = stream9::xdg::parser;

using xdg::desktop_entry_::entry_group;

BOOST_AUTO_TEST_SUITE(parse_desktop_entry_)

    BOOST_AUTO_TEST_CASE(basic_usage_)
    {
        auto const text = "[Desktop Entry]\n"
            "Version=1.0\n"
            "Type=Application\n"
            "Name=Foo Viewer\n"
            "Comment=The best viewer for Foo objects available!\n"
            "TryExec=fooview\n"
            "Exec=fooview %F\n"
            "Icon=fooview\n"
            "MimeType=image/x-foo;\n"
            "Actions=Gallery;Create;\n"
            "\n"
            "[Desktop Action Gallery]\n"
            "Exec=fooview --gallery\n"
            "Name=Browse Gallery\n"
            "\n"
            "[Desktop Action Create]\n"
            "Exec=fooview --create-new\n"
            "Name=Create a new Foo!\n"
            "Icon=fooview-new\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        BOOST_TEST(errors.size() == 0);
        BOOST_TEST(groups.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = "";

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_EXCEPTION(
            parser::desktop_entry::parse(text, groups, &errors),
            stream9::error,
            [](auto& e) {
                BOOST_TEST(e.why() == parser::desktop_entry::errc::missing_required_group);
                return true;
            }
        );

        BOOST_TEST(errors.size() == 0);
        BOOST_TEST(groups.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(missing_required_group_)
    {
        auto const text = "[foo]\n"
            "key=value\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_EXCEPTION(
            parser::desktop_entry::parse(text, groups, &errors),
            stream9::error,
            [&](auto& e) {
                BOOST_TEST(e.why() == parser::desktop_entry::errc::missing_required_group);

                return true;
            }
        );

        st9::array<entry_group, 1> expected = {
            entry_group {
                "foo", entry_group::entry_set {
                    { "key", "value" }
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        BOOST_TEST(errors.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(duplicated_group_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "[Desktop Entry]\n" // this one should be ignored
            "Type=Application\n"
            "Name=bar\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_NO_THROW(
            parser::desktop_entry::parse(text, groups, &errors)
        );

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using stream9::error;
        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::duplicated_group);
    }

    BOOST_AUTO_TEST_CASE(duplicated_key_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Name=bar\n"
            "Exec=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_NO_THROW(
            parser::desktop_entry::parse(text, groups, &errors)
        );

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using stream9::error;
        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::duplicated_key);
    }

    BOOST_AUTO_TEST_CASE(missing_required_entry_1_)
    {
        auto const text = "[Desktop Entry]\n"
            "Name=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_EXCEPTION(
            parser::desktop_entry::parse(text, groups, &errors),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == parser::desktop_entry::errc::missing_required_entry);
                return true;
            }
        );

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Name", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(missing_required_entry_2_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Link\n"
            "Name=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_EXCEPTION(
            parser::desktop_entry::parse(text, groups, &errors),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == parser::desktop_entry::errc::missing_required_entry);
                return true;
            }
        );

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Link" },
                    { "Name", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(missing_required_entry_3_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "DBusActivatable=true\n"
            "#Exec=foo\n" // it is optional when DBusActivatable is true
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "DBusActivatable", "true" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(unknown_desktop_entry_type_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == parser::desktop_entry::errc::unknown_desktop_entry_type);
    }

    BOOST_AUTO_TEST_CASE(unknown_desktop_entry_key_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Foo[en]=foo_en\n"
            "Foo=foo\n"
            "Exec=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { { "Foo", "en" }, "foo_en" },
                    { "Foo", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 2);
        BOOST_TEST(errors[0].why() == errc::unknown_desktop_entry_key);
        BOOST_TEST(errors[1].why() == errc::unknown_desktop_entry_key);
    }

    BOOST_AUTO_TEST_CASE(invalid_string_entry_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "Categories[en]=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::localized_key_on_string_entry);
    }

    BOOST_AUTO_TEST_CASE(invalid_boolean_entry_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "DBusActivatable[en]=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 2);
        BOOST_TEST(errors[0].why() == errc::localized_key_on_boolean_entry);
        BOOST_TEST(errors[1].why() == errc::invalid_boolean_value);
    }

    BOOST_AUTO_TEST_CASE(entry_type_mismatch_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Link\n"
            "Name=foo\n"
            "URL=http://localhost\n"
            "DBusActivatable=true\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Link" },
                    { "Name", "foo" },
                    { "URL", "http://localhost" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::entry_type_mismatch);
    }

    BOOST_AUTO_TEST_CASE(localized_key_without_corresponding_default_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Link\n"
            "Name[en]=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        BOOST_CHECK_EXCEPTION(
            parser::desktop_entry::parse(text, groups, &errors),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == parser::desktop_entry::errc::missing_required_entry);
                return true;
            } );

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Type", "Link" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::localized_entry_without_default);
    }

    BOOST_AUTO_TEST_CASE(duplicated_action_key_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "Actions=foo;bar;foo;\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=foo\n"
            "Exec=foo\n"
            "\n"
            "[Desktop Action bar]\n"
            "Name=bar\n"
            "Exec=bar\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Actions", "foo;bar;foo;" },
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
            entry_group {
                "Desktop Action bar", entry_group::entry_set {
                    { "Name", "bar" },
                    { "Exec", "bar" },
                }
            },
            entry_group {
                "Desktop Action foo", entry_group::entry_set {
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::duplicated_action_key);
    }

    BOOST_AUTO_TEST_CASE(invalid_actions_key_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "Actions=foo; bar+1\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=foo\n"
            "Exec=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Actions", "foo; bar+1" },
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
            entry_group {
                "Desktop Action foo", entry_group::entry_set {
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 2);
        BOOST_TEST(errors[0].why() == errc::invalid_value);
        BOOST_TEST(errors[1].why() == errc::invalid_value);
    }

    BOOST_AUTO_TEST_CASE(unregistered_action_group_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "Actions=foo;\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=foo\n"
            "Exec=foo\n"
            "\n"
            "[Desktop Action bar]\n"
            "Name=bar\n"
            "Exec=bar\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Actions", "foo;" },
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
            entry_group {
                "Desktop Action foo", entry_group::entry_set {
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::unregistered_action_group);
    }

    BOOST_AUTO_TEST_CASE(missing_registered_action_group_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "Actions=foo;bar\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=foo\n"
            "Exec=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Actions", "foo;bar" },
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
            entry_group {
                "Desktop Action foo", entry_group::entry_set {
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::missing_registered_action_group);
    }

    BOOST_AUTO_TEST_CASE(missing_required_entry_in_action_group_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "Exec=foo\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Icon=foo\n"
            ;

        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Actions", "foo" },
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "Exec", "foo" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 3);
        BOOST_TEST(errors[0].why() == errc::missing_required_entry);
        BOOST_TEST(errors[1].why() == errc::missing_required_entry);
        BOOST_TEST(errors[2].why() == errc::missing_registered_action_group);
    }

    BOOST_AUTO_TEST_CASE(localized_key_without_corresponding_default_on_action_group_)
    {
        auto const text = "[Desktop Entry]\n"
            "Type=Application\n"
            "Name=foo\n"
            "DBusActivatable=true\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name[en]=foo\n"
            "Icon=foo\n"
            ;


        st9::array<entry_group, 1> groups;
        st9::array<stream9::error> errors;

        parser::desktop_entry::parse(text, groups, &errors);

        st9::array<entry_group, 1> expected = {
            entry_group {
                "Desktop Entry", entry_group::entry_set {
                    { "Actions", "foo" },
                    { "Type", "Application" },
                    { "Name", "foo" },
                    { "DBusActivatable", "true" },
                }
            },
        };

        BOOST_TEST(groups == expected, boost::test_tools::per_element());

        using parser::desktop_entry::errc;

        BOOST_REQUIRE(errors.size() == 3);
        BOOST_TEST(errors[0].why() == errc::localized_entry_without_default);
        BOOST_TEST(errors[1].why() == errc::missing_required_entry);
        BOOST_TEST(errors[2].why() == errc::missing_registered_action_group);
    }

BOOST_AUTO_TEST_SUITE_END() // parse_desktop_entry_

} // namespace testing
