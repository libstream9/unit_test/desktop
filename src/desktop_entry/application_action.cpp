#include <stream9/xdg/desktop_entry/application_action.hpp>

#include "../namespace.hpp"
#include "../stream.hpp"

#include <stream9/xdg/desktop_entry.hpp>

#include "../../src/parser.hpp"
#include "../../src/scanner/desktop_entry_key.hpp"

#include <string_view>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/tmpfstream.hpp>
#include <stream9/test/scoped_locale.hpp>

namespace testing {

static xdg::desktop_entry
parse_entry(std::string_view const text)
{
    fs::tmpfstream os;
    os << text << std::flush;

    return { "foo.desktop", os.path().c_str() };
}

BOOST_AUTO_TEST_SUITE(application_action_)

    BOOST_AUTO_TEST_CASE(basic_usage_)
    {
        auto text = "[Desktop Entry]\n"
            "Name=foo\n"
            "Type=Application\n"
            "Exec=foo\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=Foo\n"
            "Icon=foo-icon\n"
            "Exec=foo -foo\n"
            ;

        auto const& entry = parse_entry(text);
        auto const o_action = entry.application_action("foo");

        BOOST_TEST_REQUIRE(o_action.has_value());

        BOOST_TEST(o_action->key_name() == "foo");
        BOOST_TEST(o_action->name() == "Foo");
        BOOST_TEST(o_action->icon() == "foo-icon");
        BOOST_TEST(o_action->exec() == "foo -foo");
    }

    BOOST_AUTO_TEST_CASE(localized_name_)
    {
        auto text = "[Desktop Entry]\n"
            "Name=foo\n"
            "Type=Application\n"
            "Exec=foo\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=Foo\n"
            "Name[en]=Foo(en)\n"
            "Icon=foo-icon\n"
            "Exec=foo -foo\n"
            ;

        auto const& entry = parse_entry(text);
        auto const o_action = entry.application_action("foo");

        BOOST_TEST_REQUIRE(o_action.has_value());

        {
            test::scoped_locale l { LC_MESSAGES, "C" };

            BOOST_TEST(o_action->name() == "Foo");
            BOOST_TEST(o_action->name("en_US.UTF-8") == "Foo(en)");
        }
        {
            test::scoped_locale l { LC_MESSAGES, "en_US.UTF-8" };

            BOOST_TEST(o_action->name() == "Foo(en)");
        }
    }

    BOOST_AUTO_TEST_CASE(localized_icon_)
    {
        auto text = "[Desktop Entry]\n"
            "Name=foo\n"
            "Type=Application\n"
            "Exec=foo\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=Foo\n"
            "Icon=foo-icon\n"
            "Icon[en]=foo-icon-en\n"
            "Exec=foo -foo\n"
            ;

        auto const& entry = parse_entry(text);
        auto const o_action = entry.application_action("foo");

        BOOST_TEST_REQUIRE(o_action.has_value());

        {
            test::scoped_locale l { LC_MESSAGES, "C" };

            BOOST_TEST(o_action->icon() == "foo-icon");
            BOOST_TEST(o_action->icon("en_US.UTF-8") == "foo-icon-en");
        }
        {
            test::scoped_locale l { LC_MESSAGES, "en_US.UTF-8" };

            BOOST_TEST(o_action->icon() == "foo-icon-en");
        }
    }

    BOOST_AUTO_TEST_CASE(contains_)
    {
        auto text = "[Desktop Entry]\n"
            "Name=foo\n"
            "Type=Application\n"
            "Exec=foo\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=Foo\n"
            "Icon=foo-icon\n"
            "Icon[en]=foo-icon(en)\n"
            "Exec=foo -foo\n"
            "X-Foo=x-foo\n"
            ;

        auto const& entry = parse_entry(text);
        auto const o_action = entry.application_action("foo");

        BOOST_TEST_REQUIRE(o_action.has_value());

        BOOST_TEST(o_action->contains("Icon"));
        BOOST_TEST(o_action->contains("Icon[en_US.UTF-8]"));
        BOOST_TEST(o_action->contains("X-Foo"));

        BOOST_CHECK_EXCEPTION(
            o_action->contains(""),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == xdg::errc::parse_error);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(value_)
    {
        auto text = "[Desktop Entry]\n"
            "Name=foo\n"
            "Type=Application\n"
            "Exec=foo\n"
            "Actions=foo\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name=Foo\n"
            "Name[en]=Foo(en)\n"
            "Icon=foo-icon\n"
            "Exec=foo -foo\n"
            "X-Foo=x\\\\foo\n"
            ;

        auto const& entry = parse_entry(text);
        auto const o_action = entry.application_action("foo");

        BOOST_TEST_REQUIRE(o_action.has_value());

        BOOST_TEST(o_action->value("Name") == "Foo");
        BOOST_TEST(o_action->value("Name[en]") == "Foo(en)");
        BOOST_TEST(o_action->value("X-Foo") == "x\\foo");

        BOOST_CHECK_EXCEPTION(
            o_action->value("Icon;"),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == xdg::errc::parse_error);
                return true;
            }
        );
    }

BOOST_AUTO_TEST_SUITE_END() // application_action_

} // namespace testing
