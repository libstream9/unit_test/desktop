#include <stream9/xdg/desktop_entry/locale.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace desktop = stream9::xdg;

using desktop::desktop_entry_::locale;

BOOST_AUTO_TEST_SUITE(locale_)

    BOOST_AUTO_TEST_CASE(default_construction_)
    {
        locale l;

        BOOST_TEST(l.empty());
        BOOST_TEST(l.lang().empty());
        BOOST_TEST(l.country().empty());
        BOOST_TEST(l.encoding().empty());
        BOOST_TEST(l.modifier().empty());
    }

    BOOST_AUTO_TEST_CASE(construct_from_literal_)
    {
        locale l = "ja_JP.utf8@mod";

        BOOST_TEST(l == "ja_JP.utf8@mod");
        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "JP");
        BOOST_TEST(l.encoding() == "utf8");
        BOOST_TEST(l.modifier() == "mod");
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_view_)
    {
        std::string_view s = "ja_JP.utf8@mod";
        locale l = s;

        BOOST_TEST(l == s);
        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "JP");
        BOOST_TEST(l.encoding() == "utf8");
        BOOST_TEST(l.modifier() == "mod");
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_)
    {
        std::string s = "ja_JP.utf8@mod";
        locale l = s;

        BOOST_TEST(l == s);
        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "JP");
        BOOST_TEST(l.encoding() == "utf8");
        BOOST_TEST(l.modifier() == "mod");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_1_)
    {
        locale l = "ja.utf8@mod";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "");
        BOOST_TEST(l.encoding() == "utf8");
        BOOST_TEST(l.modifier() == "mod");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_2_)
    {
        locale l = "ja_JP@mod";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "JP");
        BOOST_TEST(l.encoding() == "");
        BOOST_TEST(l.modifier() == "mod");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_3_)
    {
        locale l = "ja_JP.utf8";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "JP");
        BOOST_TEST(l.encoding() == "utf8");
        BOOST_TEST(l.modifier() == "");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_4_)
    {
        locale l = "ja@mod";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "");
        BOOST_TEST(l.encoding() == "");
        BOOST_TEST(l.modifier() == "mod");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_5_)
    {
        locale l = "ja_JP";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "JP");
        BOOST_TEST(l.encoding() == "");
        BOOST_TEST(l.modifier() == "");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_6_)
    {
        locale l = "ja.utf8";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "");
        BOOST_TEST(l.encoding() == "utf8");
        BOOST_TEST(l.modifier() == "");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_7_)
    {
        locale l = "ja";

        BOOST_TEST(l.lang() == "ja");
        BOOST_TEST(l.country() == "");
        BOOST_TEST(l.encoding() == "");
        BOOST_TEST(l.modifier() == "");
    }

    BOOST_AUTO_TEST_CASE(empty_sections_8_)
    {
        BOOST_CHECK_THROW(
            { locale { "_JP" }; },
            stream9::error
        );
    }

BOOST_AUTO_TEST_SUITE_END() // locale_

} // namespace testing
