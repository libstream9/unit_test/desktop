#include "create_directory_tree.hpp"

#include <cassert>

#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/fstream.hpp>
#include <stream9/json.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/string_view.hpp>

namespace testing {

using st9::string_view;

using st9::path::operator/;

static void
create_file(string_view path, string_view line)
{
    st9::ofstream os { path };

    os << line << "\n";
}

static void
create_file(string_view path, json::array const& lines)
{
    st9::ofstream os { path };

    for (auto const& v: lines) {
        if (v.is_string()) {
            os << v.get_string().c_str() << "\n";
        }
    }
}

void
create_directory_tree(string_view parent, json::object const& tree)
{
    fs::mkdir_recursive(parent);

    for (auto const& [name, value]: tree) {
        auto path = parent / name;

        if (value.is_string()) {
            create_file(path, value.get_string());
        }
        else if (value.is_array()) {
            create_file(path, value.get_array());
        }
        else if (value.is_object()) {
            create_directory_tree(path, value.get_object());
        }
    }
}

} // namespace testing
