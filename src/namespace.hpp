#ifndef STREAM9_DESKTOP_TEST_NAMESPACE_HPP
#define STREAM9_DESKTOP_TEST_NAMESPACE_HPP

namespace stream9::xdg {}
namespace stream9::xdg::applications {}
namespace stream9::filesystem {}
namespace stream9::json {}
namespace stream9::log {}
namespace stream9::strings {}
namespace stream9::test {}
namespace stream9::path {}

namespace testing {

namespace st9 = stream9;
namespace xdg = st9::xdg;
namespace apps = xdg::applications;
namespace fs = st9::filesystem;
namespace log = st9::log;
namespace str = st9::strings;
namespace test = st9::test;
namespace json = st9::json;
namespace path = st9::path;

} // namespace testing

#endif // STREAM9_DESKTOP_TEST_NAMESPACE_HPP
